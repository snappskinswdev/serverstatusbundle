<?php

namespace Snappskin\ServerStatusBundle\Utils;
/**
 * Created by PhpStorm.
 * User: Pavel Hudinsky
 * Email: p.hudinsky@gmail.com
 * Date: 9/14/2015
 * Time: 3:06 PM
 */
class ServerStatistic
{
    public function cpuUsage()
    {
        $usage = exec("grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage \"%\"}'");

        return $usage;
    }

    public function memoryUsage()
    {
        $usage = exec("df -h | grep rootfs | awk '{print $5}'", $output);

        return $usage;
    }
}