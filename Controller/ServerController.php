<?php

namespace Snappskin\ServerStatusBundle\Controller;

use Doctrine\DBAL\DBALException;
use Snappskin\ServerStatusBundle\Utils\ServerStatistic;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ServerController extends Controller
{
    public function statisticAction()
    {
        $statisticServer = new ServerStatistic();
        $statistic = [
            'cpu' => $statisticServer->cpuUsage(),
            'memory' => $statisticServer->memoryUsage()
        ];

        $dbAvailable = true;
        try {
            $this->getDoctrine()->getManager()->getConnection()->ping();
        } catch (DBALException $e) {
            $dbAvailable = false;
        }
        $statistic['db'] = $dbAvailable;

        return new JsonResponse($statistic);
    }
}
